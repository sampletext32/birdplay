package Mp3Decoder;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class MyConverterWrapper {

    //All info here https://github.com/josephx86/JavaMP3

    public static byte[] convert(byte[] mp3Data) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Sound sound = new Sound(new ByteArrayInputStream(mp3Data));
            sound.decodeFullyInto(outputStream);
            return convertPCMToWav(outputStream.toByteArray(), sound.isStereo() ? 2 : 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static byte[] convertPCMToWav(byte[] pcmData, int channels) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        AudioInputStream audioInputStream = new AudioInputStream(new ByteArrayInputStream(pcmData), new AudioFormat(44100, 16, channels, true, false), pcmData.length);
        try {
            AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }
}
