package NGENWAV;

public class PCM {

    public float[][] data;

    public PCM(float[] samples, int numChannels) {
        data = new float[numChannels][samples.length / numChannels];
        for (int i = 0; i < samples.length - samples.length % 2; i++) {
            data[i % numChannels][i / numChannels] = samples[i];
        }
    }
}
