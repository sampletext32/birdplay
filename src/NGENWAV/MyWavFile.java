package NGENWAV;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class MyWavFile {
    //Done, using https://audiocoding.ru/articles/2008-05-22-wav-file-structure/

    //TODO: ENCAPSULATION FIX !IMPORTANT

    /*0  -  3*/ /* 4 */ public String chunkId;//содержит символы "RIFF"
    /*4  -  7*/ /* 4 */ public int chunkSize;//размер оставшейся цепочки - размер файла минус 8 байт
    /*8  - 11*/ /* 4 */ public String format;//содержит символы "WAVE"
    /*12 - 15*/ /* 4 */ public String subchunk1Id;//содержит символы "fmt "
    /*16 - 19*/ /* 4 */ public int subchunk1Size;//16 для PCM - размер оставшейся подцепочки
    /*20 - 21*/ /* 2 */ public short audioFormat;//1 для PCM TODO: extend https://audiocoding.ru/articles/2008-05-22-wav-file-structure/wav_formats.txt
    /*22 - 23*/ /* 2 */ public short numChannels;//количество каналов
    /*24 - 27*/ /* 4 */ public int sampleRate;//частота дискретизации
    /*28 - 31*/ /* 4 */ public int byteRate;//количество байт в секунду воспроизведения
    /*32 - 33*/ /* 2 */ public short blockAlign;//байт на 1 сэмпл включая все каналы
    /*34 - 35*/ /* 2 */ public short bitsPerSample;//количество бит в сэмпле или глубина кодирования
    /*36 - 39*/ /* 4 */ public String subchunk2Id;//содержит символы "data"
    /*40 - 43*/ /* 4 */ public int subchunk2Size;//количество байт в области данных

    /*44 -inf*/ /* 0 */ byte[] data;//сами WAV данные

    public MyWavFile(byte[] wavBytes) {

        //TODO: IMPROVE PERFORMANCE BY EXCLUDING STRING CONSTRUCTION

        ByteBuffer buffer = ByteBuffer.wrap(wavBytes).order(ByteOrder.LITTLE_ENDIAN);
        chunkId = "" + (char) buffer.get() + (char) buffer.get() + (char) buffer.get() + (char) buffer.get(); // 0x52494646
        chunkSize = buffer.getInt();
        format = "" + (char) buffer.get() + (char) buffer.get() + (char) buffer.get() + (char) buffer.get();// 0x57415645
        subchunk1Id = "" + (char) buffer.get() + (char) buffer.get() + (char) buffer.get() + (char) buffer.get();// 0x666d7420
        subchunk1Size = buffer.getInt();
        audioFormat = buffer.getShort();
        numChannels = buffer.getShort();
        sampleRate = buffer.getInt();
        byteRate = buffer.getInt();
        blockAlign = buffer.getShort();
        bitsPerSample = buffer.getShort();
        subchunk2Id = "" + (char) buffer.get() + (char) buffer.get() + (char) buffer.get() + (char) buffer.get();// 0x64617461
        subchunk2Size = buffer.getInt();
        data = new byte[buffer.remaining()];
        buffer.get(data);
    }

    @Override
    public String toString() {
        return "MyWavFile{" +
                "chunkId='" + chunkId + '\'' +
                ", chunkSize=" + chunkSize +
                ", format='" + format + '\'' +
                ", subchunk1Id='" + subchunk1Id + '\'' +
                ", subchunk1Size=" + subchunk1Size +
                ", audioFormat=" + audioFormat +
                ", numChannels=" + numChannels +
                ", sampleRate=" + sampleRate +
                ", byteRate=" + byteRate +
                ", blockAlign=" + blockAlign +
                ", bitsPerSample=" + bitsPerSample +
                ", subchunk2Id='" + subchunk2Id + '\'' +
                ", subchunk2Size=" + subchunk2Size +
                ", data=" + data.length +
                '}';
    }
}
