package NGENWAV;

import OLDGENWAV.AudioReader;

import java.nio.file.Paths;

public class FileLoader {

    public static byte[] load_44100_8_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 8-int.wav");
    }
    public static byte[] load_44100_16_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 16-int.wav");
    }
    public static byte[] load_44100_24_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 24-int.wav");
    }
    public static byte[] load_44100_32_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 32-int.wav");
    }
    public static byte[] load_44100_32_float() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 32-float.wav");
    }
    public static byte[] load_44100_64_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 64-int.wav");
    }
    public static byte[] load_44100_64_float() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 44100 64-float.wav");
    }

    public static byte[] load_48000_8_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 8-int.wav");
    }
    public static byte[] load_48000_16_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 16-int.wav");
    }
    public static byte[] load_48000_24_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 24-int.wav");
    }
    public static byte[] load_48000_32_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 32-int.wav");
    }
    public static byte[] load_48000_32_float() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 32-float.wav");
    }
    public static byte[] load_48000_64_int() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 64-int.wav");
    }
    public static byte[] load_48000_64_float() {
        return AudioReader.tryLoadAny("C:\\Projects\\BirdPlay\\media\\wavs\\Critical (VIP) 48000 64-float.wav");
    }
}
