package NGENWAV;

import NGENWAV.PCM;

public class ChannelsSplitter {
    public static PCM splitChannels(float[] samples, int channels) {
        return new PCM(samples, channels);
    }
}
