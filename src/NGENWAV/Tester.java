package NGENWAV;

import OLDGENWAV.AudioReader;
import OLDGENWAV.SoundPlayer;
import ScreenLogic.Screen;
import ScreenLogic.WaveformScreenComponent;

public class Tester {
    public static void main(String[] args) {

        //byte[] bytes = AudioReader.tryLoadAny("C:\\Users\\Admin\\Downloads\\Virtual Riot - Self Checkout.mp3");
        byte[] bytes = AudioReader.tryLoadAny("C:\\Users\\Admin\\Downloads\\modus, Loudar - No Limits (feat. Loudar).mp3");

        MyWavFile testMp3File = new MyWavFile(bytes);
        System.out.println(testMp3File);

        PCM pcm = new PCM(SamplesExtractor.extract(testMp3File), testMp3File.numChannels);

        if (false) {
            MyWavFile wav_44100_8_int = new MyWavFile(FileLoader.load_44100_8_int());
            MyWavFile wav_44100_16_int = new MyWavFile(FileLoader.load_44100_16_int());
            MyWavFile wav_44100_24_int = new MyWavFile(FileLoader.load_44100_24_int());
            MyWavFile wav_44100_32_int = new MyWavFile(FileLoader.load_44100_32_int());
            MyWavFile wav_44100_32_float = new MyWavFile(FileLoader.load_44100_32_float());
            MyWavFile wav_44100_64_int = new MyWavFile(FileLoader.load_44100_64_int());
            MyWavFile wav_44100_64_float = new MyWavFile(FileLoader.load_44100_64_float());

            float[] samples_44100_8 = SamplesExtractor.extract(wav_44100_8_int);
            float[] samples_44100_16 = SamplesExtractor.extract(wav_44100_16_int);
            float[] samples_44100_24 = SamplesExtractor.extract(wav_44100_24_int);
            float[] samples_44100_32 = SamplesExtractor.extract(wav_44100_32_int);
            float[] samples_44100_64 = SamplesExtractor.extract(wav_44100_64_int);

            PCM pcm_44100_8 = new PCM(samples_44100_8, wav_44100_8_int.numChannels);
            PCM pcm_44100_16 = new PCM(samples_44100_16, wav_44100_16_int.numChannels);
            PCM pcm_44100_24 = new PCM(samples_44100_24, wav_44100_24_int.numChannels);
            PCM pcm_44100_32 = new PCM(samples_44100_32, wav_44100_32_int.numChannels);
            PCM pcm_44100_64 = new PCM(samples_44100_64, wav_44100_64_int.numChannels);
        }

        Screen screen = new Screen(1280, 720, 60);
        screen.runLoopAsync();
        //screen.addScreenComponent(new MySingleColorScreenComponent(0, 0, 100, 100, Color.red));

        WaveformScreenComponent waveformScreenComponent = new WaveformScreenComponent(0, 0, 1280, 720, pcm);
        screen.addScreenComponent(waveformScreenComponent);
        waveformScreenComponent.prepare();

        SoundPlayer player = new SoundPlayer();
        player.prepare(44100);

        player.play(pcm.data[0], pcm.data[1]);

        player.close();
        screen.stop();

        System.out.println(1);
    }
}
