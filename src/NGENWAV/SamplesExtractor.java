package NGENWAV;

public class SamplesExtractor {

    public static float[] extract(MyWavFile wavFile) {
        byte[] data = wavFile.data;
        int length = data.length;
        float[] samples;
        switch (wavFile.bitsPerSample) {
            case 8:
                samples = new float[length];
                for (int i = 0; i < samples.length; i++) {
                    int sample = (int) (data[i * 1 + 0] & 0xff);
                    samples[i] = (float) (sample / Math.pow(2, 8));
                }

                break;
            case 16:
                samples = new float[length / 2];
                for (int i = 0; i < samples.length; i++) {
                    short sample = (short) (
                            ((data[i * 2 + 0] & 0xff) << 0) | ((data[i * 2 + 1] & 0xff) << 8));
                    samples[i] = (float) (sample / Math.pow(2, 16));
                }
                break;
            case 24:
                samples = new float[length / 3];
                for (int i = 0; i < samples.length; i++) {
                    //т.к. 24 бит влезает только в 32 бит число, записываем как есть и смещаем всё на 8 бит вправо.
                    int sample
                            = (((data[i * 3 + 0] & 0xff) << 0) | ((data[i * 3 + 1] & 0xff) << 8) | ((data[i * 3 + 2] & 0xff) << 16)) << 8;
                    samples[i] = (float) (sample / Math.pow(2, 32));//Делим на 2^32 - максимальное число в 32 битном числе, а не 24.
                }
                break;
            case 32:
                samples = new float[length / 4];
                for (int i = 0; i < samples.length; i++) {
                    int sample
                            = ((data[i * 4 + 0] & 0xff) << 0) | ((data[i * 4 + 1] & 0xff) << 8) | ((data[i * 4 + 2] & 0xff) << 16) | ((data[i * 4 + 3] & 0xff) << 24);
                    samples[i] = (float) (sample / Math.pow(2, 32));
                }
                break;
            case 64:
                samples = new float[length / 8];
                for (int i = 0; i < samples.length; i++) {
                    long sample =
                            ((long) (data[i * 8 + 0] & 0xff) << 0) | ((long) (data[i * 8 + 1] & 0xff) << 8) | ((long) (data[i * 8 + 2] & 0xff) << 16) | ((long) (data[i * 8 + 3] & 0xff) << 24) |
                                    ((long) (data[i * 8 + 4] & 0xff) << 32) | ((long) (data[i * 8 + 5] & 0xff) << 40) | ((long) (data[i * 8 + 6] & 0xff) << 48) | ((long) (data[i * 8 + 7] & 0xff) << 56);
                    samples[i] = (float) (sample / Math.pow(2, 64));
                }
                break;
            default:
                throw new RuntimeException("Unknown BitDepth");
        }
        return samples;
    }
}
