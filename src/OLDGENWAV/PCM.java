package OLDGENWAV;

public class PCM {
    public float[] leftChannel;
    public float[] rightChannel;

    public PCM() {

    }

    public PCM(WavFile wavFile) {
        switch (wavFile.channels) {
            case 1:
                //если записано МОНО, оба канала = исходному
                leftChannel = new float[wavFile.samplesCount];
                rightChannel = new float[wavFile.samplesCount];
                for (int i = 0; i < wavFile.samplesCount; i++) {
                    //записываем сэмплы
                    leftChannel[i] = wavFile.samples[i];
                    rightChannel[i] = wavFile.samples[i];
                }
                break;
            case 2:
                //если записано СТЕРЕО
                //создаём 2 массива на левый и правый канал с количеством сэмплов
                leftChannel = new float[wavFile.samplesCount];
                rightChannel = new float[wavFile.samplesCount];
                for (int i = 0, s = 0; i < wavFile.samplesCount; i++) {
                    //записываем сэмплы
                    leftChannel[i] = wavFile.samples[s++];
                    rightChannel[i] = wavFile.samples[s++];
                }

                break;
            default:
                throw new RuntimeException("Unknown Channels");
        }
    }
}
