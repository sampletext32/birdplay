package OLDGENWAV;

import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class Mp3Converter {

    private ByteArrayInputStream input;
    private AudioFormat targetAudioFormat;

    private Mp3Converter(ByteArrayInputStream input) {
        this.input = input;
    }

    public static Mp3Converter convertFrom(ByteArrayInputStream input) {
        return new Mp3Converter(input);
    }

    private void to(ByteArrayOutputStream output) {
        try (final ByteArrayOutputStream rawOutputStream = new ByteArrayOutputStream()) {
            convert(input, rawOutputStream);

            final byte[] rawResult = rawOutputStream.toByteArray();//At this step - raw result is PCM
            final AudioInputStream audioInputStream = new AudioInputStream(new ByteArrayInputStream(rawResult), new AudioFormat(44100, 16, 2, true, false), rawResult.length);
            AudioSystem.write(audioInputStream, Type.WAVE, output);
        } catch (Exception e) {
            throw new ConversionException(e);
        }
    }

    public byte[] toByteArray() {
        try (final ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            to(output);
            return output.toByteArray();
        } catch (IOException e) {
            throw new ConversionException(e);
        }
    }

    //Converts inputStream To PCM stream
    private void convert(ByteArrayInputStream mp3InputStream, ByteArrayOutputStream pcmOutputStream) {

        try {
            AudioInputStream rawAudioStream = AudioSystem.getAudioInputStream(mp3InputStream);
            AudioFormat sourceAudioFormat = rawAudioStream.getFormat();
            AudioFormat destAudioFormat = new AudioFormat(
                    AudioFormat.Encoding.PCM_SIGNED,
                    sourceAudioFormat.getSampleRate(),
                    16,
                    sourceAudioFormat.getChannels(),

                    sourceAudioFormat.getChannels() * 2,
                    sourceAudioFormat.getSampleRate(),
                    false);

            AudioInputStream sourcePCMStream = AudioSystem.getAudioInputStream(destAudioFormat, rawAudioStream);
            AudioInputStream convertStream = AudioSystem.getAudioInputStream(new AudioFormat(sourcePCMStream.getFormat().getSampleRate(), 16, sourcePCMStream.getFormat().getChannels(), true, false), sourcePCMStream);
            int read;
            byte[] buffer = new byte[8192];
            while ((read = convertStream.read(buffer, 0, buffer.length)) >= 0) {
                pcmOutputStream.write(buffer, 0, read);
            }
        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }
    }
}
