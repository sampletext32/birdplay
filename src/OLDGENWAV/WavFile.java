package OLDGENWAV;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class WavFile {
    int channels;
    int sampleRate;
    int samplesCount;
    float[] samples;
    private int chunkId;
    private int fileSize;
    private int riffType;
    private int fmtId;
    private int fmtSize;
    private int fmtExtraSize;
    private int fmtCode;
    private int byteRate;
    private int fmtBlockAlign;
    private int bitDepth;
    private int dataId;
    private int bytesLength;
    private int bytesForSample;

    private static long bytesToLong(byte[] b, int offset) {
        long result = 0;
        for (int i = offset; i < offset + 8; i++) {
            result <<= 8;
            result |= (b[i] & 0xFF);
        }
        return result;
    }

    private static float bytesToFloat(byte[] b, int offset) {

        int result = 0;
        for (int i = offset; i < offset + 4; i++) {
            result <<= 8;
            result |= (b[i] & 0xFF);
        }

        return Float.intBitsToFloat(result);
    }

    private static float bytesToShort(byte[] data, int offset) {

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.put(data[offset]);
        bb.put(data[offset + 1]);
        return bb.getShort(0);
    }

    static WavFile readWav(byte[] wavBytes) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(wavBytes);
        DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
        return readWav(dataInputStream);
    }

    //функция читает Wav файл из потока
    private static WavFile readWav(DataInputStream reader) {
                WavFile wavFile = new WavFile();
        try {
            wavFile.chunkId = Integer.reverseBytes(reader.readInt());
            wavFile.fileSize = Integer.reverseBytes(reader.readInt());
            wavFile.riffType = Integer.reverseBytes(reader.readInt());

            wavFile.fmtId = Integer.reverseBytes(reader.readInt());
            wavFile.fmtSize = Integer.reverseBytes(reader.readInt());
            wavFile.fmtCode = Short.reverseBytes(reader.readShort());
            wavFile.channels = Short.reverseBytes(reader.readShort());
            wavFile.sampleRate = Integer.reverseBytes(reader.readInt());
            wavFile.byteRate = Integer.reverseBytes(reader.readInt());
            wavFile.fmtBlockAlign = Short.reverseBytes(reader.readShort());
            wavFile.bitDepth = Short.reverseBytes(reader.readShort());

            if (wavFile.fmtSize == 18) {
                wavFile.fmtExtraSize = reader.readShort();
                for (int i = 0; i < wavFile.fmtExtraSize; i++) {
                    reader.readByte();
                }
            }

            wavFile.dataId = Integer.reverseBytes(reader.readInt());

            wavFile.bytesLength = reader.readInt();

            byte[] data = new byte[wavFile.bytesLength];
            reader.read(data, 0, wavFile.bytesLength);

            wavFile.bytesForSample =
                    wavFile.bitDepth / 8;

            wavFile.samplesCount =
                    wavFile.bytesLength / wavFile.bytesForSample / wavFile.channels;

            float[] asFloat;
            switch (wavFile.bitDepth) {
                case 24:

                    asFloat = new float[data.length / 3];

                    for (int i = 0; i < data.length; i += 3) {
                        asFloat[i / 3] = ((data[i] & 0xFF) << 8 | (data[i + 1] & 0xFF) << 16 | (data[i + 2] & 0xFF) << 24) / 2147483648f;
                    }

                    break;
                case 64:

                    asFloat = new float[data.length / 8];

                    for (int i = 0; i < data.length; i += 8) {
                        asFloat[i / 8] = (float) (bytesToLong(data, i) / ((double) Long.MAX_VALUE));
                    }

                    break;
                case 32:

                    asFloat = new float[data.length / 4];

                    for (int i = 0; i < data.length; i += 4) {
                        asFloat[i / 4] = (bytesToFloat(data, i)) / 8f;
                    }

                    break;
                case 16:

                    asFloat = new float[data.length / 2];

                    for (int i = 0; i < data.length; i += 2) {
                        asFloat[i / 2] = (bytesToShort(data, i)) / 32768f;
                    }

                    break;
                default:
                    throw new RuntimeException("Unknown BitDepth");
            }
            wavFile.samples = asFloat;

        } catch (Exception ex) {
            return null;
        }

        return wavFile;
    }

    public static byte[] shortToByteArray(short data) {
        return new byte[]{(byte) (data & 0xff), (byte) ((data >>> 8) & 0xff)};
    }
}
