package OLDGENWAV;

import Mp3Decoder.MyConverterWrapper;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AudioReader {

    public static byte[] tryLoadAny(String path) throws UnsupportedOperationException{
        byte[] wavBytes;
        if (path.endsWith("wav")) {
            wavBytes = AudioReader.readWav(path);
        } else if (path.endsWith("mp3")) {
            wavBytes = AudioReader.readMp3(path);
        } else {
            throw new UnsupportedOperationException("File Format Is Not Supported: " + path.substring(path.lastIndexOf(".") + 1));
        }
        return wavBytes;
    }

    public static byte[] readWav(String path) {
        try {
            return Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] readMp3(String path) {
        try {
            byte[] allBytes = Files.readAllBytes(Paths.get(path));

            //ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(allBytes);
//
            //AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(byteArrayInputStream);
//
            //byte[] bytes = Mp3Converter.convertFrom(byteArrayInputStream).toByteArray();
            //Files.write(Paths.get(path.subSequence(0, path.lastIndexOf(".") + 1) + "wav"), bytes);

            byte[] bytes = MyConverterWrapper.convert(allBytes);

            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
