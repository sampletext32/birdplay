package OLDGENWAV;

public class Resampler {
    public static float[] downsample(float[] values, int srcSampleRate, int destSampleRate) {
        float conversion = ((float) destSampleRate / srcSampleRate);
        int totalSamples = (int) Math.ceil(values.length * conversion);
        float[] result = new float[totalSamples];
        result[0] = values[0];
        int countOver1 = 0;
        for (int i = 1; i < totalSamples - 1; i++) {
            float pos = (float) i / totalSamples;//считаем позицию в новом
            float srcPos = pos * values.length;//ищем её в исходном массиве
            int left = (int) (srcPos);//находим левый край
            int right = left + 1;//правый - следующий
            float leftNorm = (float) left / values.length;//нормализуем левый и правый край
            float rightNorm = (float) right / values.length;
            float sampleValueLeft = (pos - leftNorm) / (rightNorm - leftNorm);//ищем коэффициенты влияния левого и правого сэмплов
            float sampleValueRight = 1 - sampleValueLeft;
            float sampleLeft = sampleValueLeft * values[left];//считаем значение сэмпла
            float sampleRight = sampleValueRight * values[right];
            result[i] = (sampleLeft + sampleRight) / 2;

            result[i] = Math.min(1, result[i]);
            result[i] = Math.max(-1, result[i]);
        }
        System.out.println(countOver1);
        result[totalSamples - 1] = values[values.length - 1];
        return result;
    }
}
