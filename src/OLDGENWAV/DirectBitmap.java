package OLDGENWAV;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class DirectBitmap {
    protected BufferedImage view;
    protected int[] pixels;
    protected int width;
    protected int height;

    protected Graphics2D localGraphics;

    DirectBitmap(int width, int height) {
        this.width = width;
        this.height = height;

        view = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        pixels = ((DataBufferInt) view.getRaster().getDataBuffer()).getData();
        localGraphics = view.createGraphics();
    }

}
