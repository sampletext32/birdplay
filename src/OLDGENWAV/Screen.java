package OLDGENWAV;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

public class Screen {
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;
    private static final int DESIRED_FPS = 60;
    public static int totalRenders = 0;
    public static int totalUpdates = 0;
    public static int lastFrameFPS = 0;
    public static long launchTime = System.nanoTime();
    private static JFrame frame;
    private static boolean running;
    private static ScreenBitmap screen;
    private static BufferStrategy bufferStrategy;
    private static Graphics graphics;

    public Screen() {
        init();

        Thread thread = new Thread(Screen::loop);
        thread.start();

        Application.launch();
        try {
            while (running) {

                Thread.sleep(1);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        dispose();
    }

    private static void loop() {
        running = true;
        long lastTime = System.nanoTime();
        double framePeriod = 1_000_000_000d / DESIRED_FPS;
        float delta = 0;
        long currentTime;
        float seconds_counter = 0;

        while (running) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime);

            lastTime = currentTime;

            if (delta >= framePeriod) {

                seconds_counter += delta / 1000000000f;

                update(delta);
                totalUpdates++;

                render();
                totalRenders++;

                delta = 0;
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ignored) {
                }
            }

            if (seconds_counter > 1) {
                seconds_counter = 0;
                lastFrameFPS = totalUpdates;
                totalUpdates = 0;
                totalRenders = 0;
            }
        }
    }

    private static void init() {
        frame = new JFrame();

        //Make our frame visible.
        frame.setVisible(true);

        //Make our program shutdown when we exit out.
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set the position and size of our frame.
        //frame.setSize(new Dimension(WIDTH + 16, HEIGHT + 39));
        frame.setSize(WIDTH + (frame.getInsets().left + frame.getInsets().right), HEIGHT + (frame.getInsets().top + frame.getInsets().bottom));

        //Put our frame in the center of the screen.
        frame.setLocationRelativeTo(null);

        Canvas canvas = new Canvas();
        canvas.setSize(WIDTH, HEIGHT);

        //Add our graphics component
        frame.add(canvas);

        //Create our object for buffer strategy.
        canvas.createBufferStrategy(3);

        screen = new ScreenBitmap(WIDTH, HEIGHT);
        bufferStrategy = canvas.getBufferStrategy();
        graphics = bufferStrategy.getDrawGraphics();
    }

    private static void update(float delta) {
        Application.update();
    }

    private static void render() {
        //canvas.paint(graphics);

        Application.render(screen);

        screen.render(graphics);

        //graphics.dispose();
        bufferStrategy.show();
    }

    private static void dispose() {
        frame.dispose();
    }

    public static void stop() {
        running = false;
    }

}