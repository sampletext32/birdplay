package OLDGENWAV;

class ConversionException extends RuntimeException {
    ConversionException(Throwable cause) {
        super("Failed to convert audio data", cause);
    }
}