package OLDGENWAV;

import java.awt.*;
import java.awt.image.BufferedImage;

class ScreenBitmap extends DirectBitmap {

    private static final boolean PRINT_FPS = true;

    ScreenBitmap(int width, int height) {
        super(width, height);
    }

    void drawImage(BufferedImage image) {
        localGraphics.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
    }

    void render(Graphics graphics) {
        graphics.drawImage(view, 0, 0, view.getWidth(), view.getHeight(), null);
        if (PRINT_FPS) printFPS(graphics);
    }

    private void printFPS(Graphics graphics) {
        Color color = graphics.getColor();
        graphics.setColor(Color.GREEN);
        graphics.setFont(graphics.getFont().deriveFont(30f));
        graphics.drawString(String.valueOf(Screen.lastFrameFPS), 0, 22);
        graphics.setColor(color);
    }

}