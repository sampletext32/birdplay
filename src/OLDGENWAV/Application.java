package OLDGENWAV;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

class Application {

    private static List<BufferedImage> displayContents = new ArrayList<>();

    static void launch() {
        String songPath = "C:\\Users\\Admin\\Downloads\\NGHTMRE &amp; Virtual Riot - ID (Unreleased)_01.wav";
        //String songPath = "C:\\Users\\Admin\\Downloads\\Stabby - Shuriken_JUNE11_25.wav";
        //String songPath = "C:\\Users\\Admin\\Downloads\\Labyrinth of a Summer Day (Lewis Remix).wav";
        //String songPath = "C:\\Users\\Admin\\Downloads\\chinatsu uwu.wav";
        //String songPath = "C:\\Users\\Admin\\Downloads\\Virtual Riot - Self Checkout.mp3";
        //String songPath = "C:\\Users\\Admin\\Downloads\\Jotori - Overlook.mp3";

        byte[] wavBytes;
        if (songPath.endsWith("wav")) {
            wavBytes = AudioReader.readWav(songPath);
        } else if (songPath.endsWith("mp3")) {
            wavBytes = AudioReader.readMp3(songPath);
        } else {
           throw new UnsupportedOperationException("File Format Is Not Supported: " + songPath.substring(songPath.lastIndexOf(".") + 1));
        }
        WavFile wavFile = WavFile.readWav(wavBytes);
        if (wavFile == null) {
            Screen.stop();
            return;
        }
        PCM pcm = new PCM(wavFile);
        if (wavFile.sampleRate == 48000) {
            pcm.leftChannel = Resampler.downsample(pcm.leftChannel, 48000, 44100);
            pcm.rightChannel = Resampler.downsample(pcm.rightChannel, 48000, 44100);
            //pcm.rightChannel = OLDGENWAV.Resampler.downsample(pcm.leftChannel, 48000, 44100);
        }
        displayContents.add(prepareImage(pcm));

        SoundPlayer player = new SoundPlayer();

        player.prepare(44100);
        player.play(pcm.leftChannel, pcm.rightChannel);
        Screen.stop();
        //player.play(wavFile.samples);
    }

    static void update() {

    }

    private static BufferedImage prepareImage(float[] values) {
        BufferedImage image;
        image = new BufferedImage(Screen.WIDTH, Screen.HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        try {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Screen.WIDTH, Screen.HEIGHT);
            g.setColor(Color.red);
            int width = 1;
            //Math.max(1, OLDGENWAV.Main.WIDTH / pcm.leftChannel.length);
            int centerH = Screen.HEIGHT / 2;
            for (int i = 0; i < values.length; i++) {
                int x = (int) ((float) i / (values.length) * Screen.WIDTH);
                int valueL =
                        (int) (values[i] * (centerH) * 0.9f);
                int valueR =
                        (int) (values[i] * (centerH) * 0.9f);
                g.setColor(Color.red);
                g.fillRect(x, centerH - valueL, width, valueL);
                g.setColor(Color.green);
                g.fillRect(x, centerH, width, valueR);
            }
        } finally {
            g.dispose();
        }
        return image;
    }

    private static BufferedImage prepareImage(PCM pcm) {
        BufferedImage image;
        image = new BufferedImage(Screen.WIDTH, Screen.HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        try {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, Screen.WIDTH, Screen.HEIGHT);
            g.setColor(Color.red);
            int width = 1;
            //Math.max(1, OLDGENWAV.Main.WIDTH / pcm.leftChannel.length);
            int centerH = Screen.HEIGHT / 2;
            for (int i = 0; i < pcm.leftChannel.length; i++) {
                int x = (int) ((float) i / (pcm.leftChannel.length) * Screen.WIDTH);
                int valueL =
                        (int) (pcm.leftChannel[i] * (centerH) * 0.9f);
                int valueR =
                        (int) (pcm.rightChannel[i] * (centerH) * 0.9f);
                g.setColor(Color.red);
                g.fillRect(x, centerH - valueL, width, valueL);
                g.setColor(Color.green);
                g.fillRect(x, centerH, width, valueR);
            }
        } finally {
            g.dispose();
        }
        return image;
    }

    static void render(ScreenBitmap screenBitmap) {
        for (BufferedImage displayContent : displayContents) {
            screenBitmap.drawImage(displayContent);
        }
    }
}
