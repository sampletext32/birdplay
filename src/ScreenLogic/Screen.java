package ScreenLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Screen {
    public int width;
    public int height;

    int desired_fps;
    JFrame frame;
    BufferStrategy bufferStrategy;
    Graphics toScreenGraphics;

    boolean running;

    List<MyScreenComponent> screenComponents;
    boolean isAsync;
    Thread thread;

    public Screen(int width, int height, int desired_fps) {
        this.width = width;
        this.height = height;
        this.desired_fps = desired_fps;

        frame = new JFrame();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(width + (frame.getInsets().left + frame.getInsets().right), height + (frame.getInsets().top + frame.getInsets().bottom));
        frame.setLocationRelativeTo(null);

        Canvas canvas = new Canvas();
        canvas.setSize(width, height);
        frame.add(canvas);
        canvas.createBufferStrategy(3);

        bufferStrategy = canvas.getBufferStrategy();
        toScreenGraphics = bufferStrategy.getDrawGraphics();

        screenComponents = new ArrayList<>();
    }

    private void onRender() {
        //TODO: Remove clearing if all components stand on their places, because of self overlaying
        toScreenGraphics.clearRect(0, 0, width, height);
        for (MyScreenComponent screenComponent : screenComponents) {
            screenComponent.draw(toScreenGraphics);
        }
        bufferStrategy.show();
    }

    private void onUpdate(float dt) {
        for (MyScreenComponent screenComponent : screenComponents) {
            screenComponent.update(dt);
        }
    }

    private void loop() {
        long lastTime = System.nanoTime();
        double framePeriod = 1_000_000_000d / desired_fps;
        float delta = 0;
        long currentTime;

        while (running) {
            currentTime = System.nanoTime();
            delta += (currentTime - lastTime);

            lastTime = currentTime;

            if (delta >= framePeriod) {
                onUpdate(delta);
                delta = 0;
            }

            onRender();
        }
    }

    public void runLoopAsync() {
        if (!running) {
            running = true;
            isAsync = true;
            thread = new Thread(this::loop);
            thread.start();
        }
    }

    public void runLoopSync() {
        if (!running) {
            running = true;
            isAsync = false;
            loop();
        }
    }

    public void stop() {
        if (!running) {
            return;
        }
        if (isAsync) {
            try {
                running = false;
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            running = false;
        }
    }

    public void addScreenComponent(MyScreenComponent component) {
        screenComponents.add(component);
    }
}
