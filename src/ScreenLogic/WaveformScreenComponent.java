package ScreenLogic;

import NGENWAV.PCM;

import java.awt.*;

public class WaveformScreenComponent extends MyBufferedScreenComponent {

    PCM pcm;



    public WaveformScreenComponent(int x, int y, int width, int height, PCM pcm) {
        super(x, y, width, height);
        this.pcm = pcm;
    }

    public void prepare() {
        int centerH = height / 2;
        int bandWidth = 1;
        for (int i = 0; i < pcm.data[0].length; i++) {
            int x = (int) ((float) i / (pcm.data[0].length) * width);
            int valueL =
                    (int) (Math.abs(pcm.data[0][i]) * (centerH) * 0.9f);
            int valueR =
                    (int) (Math.abs(pcm.data[1][i]) * (centerH) * 0.9f);
            localGraphics.setColor(Color.red);
            localGraphics.fillRect(x, centerH - valueL, bandWidth, valueL);
            localGraphics.setColor(Color.green);
            localGraphics.fillRect(x, centerH, bandWidth, valueR);
        }
    }
}
