package ScreenLogic;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class MyBufferedScreenComponent extends MyScreenComponent {

    protected BufferedImage view;
    protected int[] pixels;

    protected Graphics localGraphics;

    public MyBufferedScreenComponent(int x, int y, int width, int height) {
        super(x, y, width, height);
        view = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) view.getRaster().getDataBuffer()).getData();
        localGraphics = view.createGraphics();
    }

    @Override
    public void draw(Graphics screenGraphics) {
        screenGraphics.drawImage(view, x, y, width, height, null);
    }

    @Override
    public void update(float dt) {

    }
}
