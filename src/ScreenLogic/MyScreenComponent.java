package ScreenLogic;

import java.awt.*;

public abstract class MyScreenComponent {
    int x;
    int y;
    int width;
    int height;

    public MyScreenComponent(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public abstract void draw(Graphics screenGraphics);

    public abstract void update(float dt);
}
