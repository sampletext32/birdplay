package ScreenLogic;

import java.awt.*;

public class MySingleColorScreenComponent extends MyScreenComponent {

    Color color;

    public MySingleColorScreenComponent(int x, int y, int width, int height, Color color) {
        super(x, y, width, height);
        this.color = color;
    }

    @Override
    public void draw(Graphics screenGraphics) {
        screenGraphics.setColor(color);
        screenGraphics.fillRect(x, y, width, height);
    }

    @Override
    public void update(float dt) {

    }
}
